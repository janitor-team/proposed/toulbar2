toulbar2 (1.2.0+dfsg-0.1) UNRELEASED; urgency=medium

  * NMU: New upstream version 1.2.0

 -- Thomas Schiex <thomas.schiex@toulouse.inra.fr>  Sun, 30 Oct 2022 18:13:00 +0100

toulbar2 (1.1.1+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.1

 -- Thomas Schiex <Thomas.Schiex@toulouse.inra.fr>  Tue, 19 Jan 2021 14:06:11 +0000

toulbar2 (1.1.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.0
  * Adding new dependencies to control, and new options to compilation rules
  * Removing pre-push hook, useless for debian
  * Supposedly improved watch file
  * Recovering doc
  * Avoiding version generation
  * New upstream version 1.1.0+dfsg
  * Old patches removed, git-hooks excluded
  * New upstream version 1.1.0+dfsg

 -- Thomas Schiex <Thomas.Schiex@toulouse.inra.fr>  Fri, 30 Oct 2020 10:14:29 +0000

toulbar2 (1.0.0+dfsg3-2) unstable; urgency=medium

  * Team upload.
  * debhelper 12
  * Standards-Version: 4.3.0
  * Remove trailing whitespace in debian/copyright
  * Add missing Build-Depends: pkg-config
  * Prevent generation of PDF documentation since otherwise toulbar2 does
    not build (see bug #920459).  This means should be reverted once doxygen
    is fixed.
  * toulbar2-doc: Section: doc

 -- Andreas Tille <tille@debian.org>  Mon, 18 Feb 2019 22:17:10 +0100

toulbar2 (1.0.0+dfsg3-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add the missing build dependency on zlib1g-dev. (Closes: #916715)

 -- Adrian Bunk <bunk@debian.org>  Fri, 11 Jan 2019 13:47:51 +0200

toulbar2 (1.0.0+dfsg3-1) unstable; urgency=medium

  [ Thomas Schiex ]
  * Developer documentation regenerated and removed from upstream. User,
  developer and formats documentation installed in suitable location
  and registered via doc-base in a separate toulbar2-doc package. Examples
  provided as tar.xz balls.

  [ Aaron M. Ucko ]
  * Sponsor upload to unstable.

 -- Aaron M. Ucko <ucko@debian.org>  Sun, 12 Aug 2018 23:22:14 -0400

toulbar2 (1.0.0+dfsg-1) unstable; urgency=medium

  * New major release. Includes DGVNS parallelized search method,
  new JSON compatible CFN input format that allows for negative or
  decimal costs as well as maximization, and clique cover global
  constraint (Closes: Bug#831148).

 -- Thomas Schiex <Thomas.Schiex@toulouse.inra.fr>  Thu, 28 Jun 2018 17:35:05 +0200

toulbar2 (0.9.8-1) unstable; urgency=medium

  * Initial release.
  * Closes: #780516 (ITP: toulbar2 - Exact optimization solver for Graphical Models.)

 -- Thomas Schiex <Thomas.Schiex@toulouse.inra.fr>  Thu, 28 Apr 2016 00:33:44 +0200
